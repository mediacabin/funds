var gulp = require("gulp");
var babel = require("gulp-babel");
var nodemon = require("gulp-nodemon");
var cached = require("gulp-cached");

gulp.task("copy", function() {
	return gulp.src([".env", "src/**/*.*", "!src/**/*.js"])
	 .pipe(gulp.dest("dist"));
});

gulp.task("server", ['copy'], function () {
  return gulp.src(["src/**/*.js", "!src/public/**/*.js"])
   .pipe(babel())
   .pipe(gulp.dest("dist"));
});

gulp.task("default", ['server'], function() {
    gulp.watch('src/**/*.js', ['server']);
    gulp.watch(["src/**/*.*", "!src/**/*.js"], ['copy']);

    let nodemonStream = nodemon({
        script: 'dist/keystone.js',
        ext: 'js jade',
        ignore: ['node_modules/', 'src/'],
    });
    return nodemonStream;
});