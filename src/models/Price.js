var keystone = require('keystone'),
  moment = require('moment'),
  Types = keystone.Field.Types

/**
 * Fund Model
 * ==========
 **/

var Price = new keystone.List('Price')

Price.add({
  price: { type: Types.Number, format: '0,0.00', initial: true, required: true, index: true, label: 'Price'},
  date: { type: Types.Datetime, default: Date.now, initial: true, required: true, index: true, label: 'Date and Time'},
  fund: { type: Types.Relationship, ref: 'Fund', initial: true, many: false, label: 'Share Class' },
  canc: {type: Types.Number, format: '0', initial: true},
  bid: {type: Types.Number, initial: true},
  yield: {type: Types.Text, initial: true},
})

/**
 * Registration
 */

Price.defaultColumns = 'date, fund'
Price.register()
