var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * User Model
 * ==========
 */

var Manager = new keystone.List('Manager');

Manager.add({
	name: { type: Types.Name, required: true, index: true },
	email: { type: Types.Email, initial: true, required: true, index: true },
	description: { type: Types.Textarea, initial: true, required: true, label:"Description" },
	image: {type: Types.LocalFile, dest: process.env.UPLOAD_LOCATION + '/images/managers', prefix: '/uploads/images/managers',
		filename: function (item, file) {
			return item.id + 'manager.' + file.extension;
		}
	},
});



/**
 * Registration
 */

Manager.defaultColumns = 'name, email, description';
Manager.register();
