var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * User Model
 * ==========
 */

var ManagementTeam = new keystone.List('ManagementTeam');

ManagementTeam.add({
	name: { type: Types.Text, required: true, index: true },
	people: { type: Types.Relationship, ref: 'Manager', initial: true, many: true },
	description: { type: Types.Textarea, initial: true, required: true, label:"Description" },
	image: {type: Types.LocalFile, dest: process.env.UPLOAD_LOCATION + '/images/teams', prefix: '/uploads/images/teams',
		filename: function (item, file) {
			return item.id + 'mteam.' + file.extension;
		}
	},

});


/**
 * Registration
 */

ManagementTeam.defaultColumns = 'name, people, description';
ManagementTeam.register();
