var keystone = require('keystone'),
  moment = require('moment'),
  Types = keystone.Field.Types
var path = require('path');
var fs = require('fs');

var { getModelForFund } = require('../util/getFilename');

/**
 * Fund Model
 * ==========
 **/



var Fund = new keystone.List('Fund', {
  label: 'Share Classes',
});

Fund.add({
  sedolNumber: {type: Types.Text, initial: true, unique: true, required: true, index: true, label: 'SEDOL No.'},
  name:    { type: Types.Text, initial: true, required: true, index: true, label: 'Name' },
  ISIN:    { type: Types.Text, initial: true, required: true, index: true, label: 'ISIN' },
  managementTeam: { type: Types.Relationship, ref: 'ManagementTeam', initial: true, many: false },
  summary: { type: Types.Html, wysiwyg: true, index: false, initial: true, label: 'Summary' },
  class:   { type: Types.Relationship, ref:"FundClass", required:true, initial:true, label:"Class" },
  group:   { type: Types.Relationship, ref:"FundGroup", required:true, initial:true, label:"Fund Group/Sponsor" },
  range:   { type: Types.Relationship, ref:"FundRange", required:true, initial:true, label:"Fund", filters: { from: ':group' }},
  accInc:  {type: Types.Select, initial: true, required: true, index: true, options: [
      { value: 'acc', label: 'Acc' },
      { value: 'inc', label: 'Inc' },
      { value: 'both', label: 'Both' }    
  ]},
  objective:  {type: Types.Select, initial: true, index: true, options: [
      { value: 'Growth', label: 'Growth' },
      { value: 'Income', label: 'Income' },
      { value: 'Growth and Income', label: 'Growth and Income' }    
  ]},
}, 'Fund Prices:', {
  dW: { type: Types.Select, label: 'Daily Weekly', options: [
      { value: 'D', label: 'Daily' },
      { value: 'W', label: 'Weekly' }
  ] },
  utOeic: { type: Types.Select, label: 'UT OEIC', options: [
      { value: 'UT', label: 'UT' },
      { value: 'OEIC', label: 'OIEC' }
  ] },
  Yield: {type: Number,format: '0%',initial: true},
}, 'Key Facts:', {
  launchDate: {type: Types.Date, initial: true},
  iaSector: {type: Types.Text,initial: true, label: 'IA Sector'},
  MorningstarSector: {type: Types.Text,initial: true},
  UnitType: {type: Types.Text,initial: true},
  XDDates: {type: Types.Date,initial: true,many: true, label: 'XD Dates'},
  DistributionDates: {type: Types.Date,initial: true,default: moment()},
  NisaQualifying: {type: Types.Boolean, initial: true, label: 'ISA Qualifying'}
});

/**
 * Registration
 */

Fund.defaultColumns = 'name, from';
Fund.register();
