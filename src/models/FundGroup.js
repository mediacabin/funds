var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * FundGroup Model
 * ==========
 */

var FundGroup = new keystone.List('FundGroup', {
  label: 'Fund Groups/Sponsors',
});

FundGroup.add({
	name: { type: Types.Text, required: true, index: true },
	description: { type: Types.Textarea, initial: true, label:"Description" },
  validationDate: {type: Types.Datetime, initial: true,required: true },
}, 'Misc', {
  IFSL: {type: Types.Boolean, initial: false},
  Marlborough: {type: Types.Boolean, initial: false},
  International: {type: Types.Boolean, initial: false},
  MFM: {type: Types.Boolean, initial: false},
  DUBLIN: {type: Types.Boolean, initial: false},
  color: {type: Types.Color, initial: true},
  website: {type: Types.Text},
  logo: {type: Types.LocalFile, dest: process.env.UPLOAD_LOCATION + '/images/logos', prefix: '/uploads/images/logos',
    filename: function (item, file) {
      return item.id + 'logo.' + file.extension
    }
  },
  ifslBannerImage: {type: Types.LocalFile, dest: process.env.UPLOAD_LOCATION + '/images/banners', prefix: '/uploads/images/banners',
    filename: function (item, file) {
      return item.id + 'banner.' + file.extension
    }
  }
});



/**
 * Registration
 */

FundGroup.defaultColumns = 'name, description';
FundGroup.register();
