var keystone = require('keystone'),
  moment = require('moment'),
  Types = keystone.Field.Types


/**
 * Fund Model
 * ==========
 **/

var Document = new keystone.List('Document', {
  label: 'Documents',
});

Document.add({
  name: { type: Types.Text, initial: true, required: true },
  type: { type: Types.Text, initial: true, index: true, required: true },
  date: { type: Types.Datetime, default: Date.now, initial: true, required: true, index: true, label: 'Date and Time'},
  fund: {      type: Types.Relationship, ref: 'Fund',      many: false, label: 'Share Class' },
  fundRange: { type: Types.Relationship, ref: 'FundRange', many: false, label: 'Fund' },
  sponsor: {   type: Types.Relationship, ref: 'FundGroup', many: false, label: 'Sponsor' },
  category: { type: Types.Text },
});

/**
 * Registration
 */

Document.defaultColumns = 'name';
Document.register();
