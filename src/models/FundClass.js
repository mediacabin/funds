var keystone = require('keystone'),
  moment = require('moment'),
  Types = keystone.Field.Types

/**
 * Fund Model
 * ==========
 **/

var FundClass = new keystone.List('FundClass', {
  label: 'Classes',
});

FundClass.add({
  name: {type: Types.Text, initial: true, required: true, index: true, label: 'Name.'},
});

/**
 * Registration
 */

FundClass.defaultColumns = 'name'
FundClass.register()
