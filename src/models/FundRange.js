var keystone = require('keystone'),
  moment = require('moment'),
  fs = require('fs'),
  path = require('path'),
  Types = keystone.Field.Types

/**
 * Fund Model
 * ==========
 **/

function doesExist(path) {
  try {
    let stats = fs.lstatSync(path);
    if (stats.isFile()) {
        return true;
    }
    return false;
  } catch (e) {
    return false;
  }
  return false;
}

function getFilename (item, file) {
  let filetry = 1;
  let name = 'error'
  do {
    let suffix = filetry > 1 ? filetry : '';
    name = path.basename(file.originalname, '.' + file.extension) + suffix + '.' + file.extension;
    filetry++;
  } while(doesExist(process.env.UPLOAD_LOCATION + '/documents/' + name));
  return name;
}

var FundRange = new keystone.List('FundRange', {
  label: 'Funds',
})

FundRange.add({
  name: {type: Types.Text, initial: true, required: true, index: true, label: 'Name'},
  rangeID: { type: Types.Text, initial: true, unqiue: true,  required: true, index: true, label: 'ID' },
  from: { type: Types.Relationship, ref: "FundGroup", required: true, initial: true, label: "Fund Group/Sponsor" },
  marlboroughCategory: { type: Types.Text, initial: true },
  documentCategories: { type: Types.TextArray, initial: false },
});

/**
 * Registration
 */

FundRange.defaultColumns = 'name, ID'
FundRange.register()
