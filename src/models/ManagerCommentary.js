var keystone = require('keystone');
var Types = keystone.Field.Types;

var ManagerCommentary = new keystone.List('ManagerCommentary');

ManagerCommentary.add({
  title:       { type: Types.Text },
  content:     { type: Types.Text },
  range:       { type: Types.Relationship, ref:"FundRange", required:true, initial:true, label:"Fund" },
  status:      { type: Types.Text },
  pdf:         { type: Types.Text },
  publishedAt: { type: Types.Datetime },
});

ManagerCommentary.schema.pre('save', function(next) {
    if (!this.publishedAt) {
        this.publishedAt = new Date();
    }
    next();
});

ManagerCommentary.register();
