var path = require('path');
var fs = require('fs');

export default function(path) {
  try {
    let stats = fs.lstatSync(path);
    if (stats.isFile()) {
        return true;
    }
    return false;
  } catch (e) {
    return false;
  }
  return false;
}
