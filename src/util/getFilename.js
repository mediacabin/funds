var keystone = require('keystone');
const Types = keystone.Field.Types;

export function getFilenameForRange(rangeName, docType, extension) {
  return process.env.UPLOAD_LOCATION + '/documents/fund' + rangeName + ' ' + docType + '.' + extension;
}

export function getFilenameForFund(fundName, docType, extension) {
  return process.env.UPLOAD_LOCATION + '/documents/shareclass' + fundName + ' ' + docType + '.' + extension;
}


export function getModelForFund(docType) {
  return {
    type: Types.LocalFile,
    dest: process.env.UPLOAD_LOCATION + '/documents/fund',
    prefix: '/uploads/documents/fund',
    filename: (item, file) => item.name + ' ' + docType + '.' + file.extension,
  };
}

