require("babel-polyfill");

require('dotenv').load();

import keystone from 'keystone';

keystone.init({
	'name': 'Funds',
	'brand': 'Funds',

  	static: [ process.env.PUBLIC_LOCATION, "/app/dist/public", "/app/src/public" ],
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': 'jade',
	
	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',
	'port': process.env.PORT || 3000,
	'cookie secret': process.env.COOKIE_SECRET || 'Usasfddfa41F23421sfdaer',
	'mongo': process.env.MONGO_URI || "mongodb://localhost/funds"
});

keystone.import('models');

keystone.set('locals', {
	_: require('lodash'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable
});

keystone.set('routes', require('./routes'));

keystone.set('nav', {
	'users': 'users'
});

keystone.start();
