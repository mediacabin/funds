import dateFormat from 'dateformat';
import _ from 'lodash';

var keystone = require('keystone');
var Sponsors = keystone.list('FundGroup');
var Funds = keystone.list('Fund');
var Fundrange = keystone.list('FundRange');
var FundClass = keystone.list('FundClass');
var Manager = keystone.list('Manager');
var ManagerCommentary = keystone.list('ManagerCommentary');
var Price = keystone.list('Price');
var Document = keystone.list('Document');
var ManagementTeam = keystone.list('ManagementTeam');
var fs = require('fs')

async function getClassName(classID) {
    const fundClass = await FundClass.model.findById(classID).exec();
    return fundClass.name;
}
async function getManagersComments(rangeID) {
     const managersComments = await ManagerCommentary.model.find().where('range', rangeID).sort('-publishedAt').limit(1).exec();
    return managersComments;
}

async function getAllPrices() {
  const fundPrice = await Price.model.find().exec();
  console.log('===== ALL PRICES ====');
  console.log(fundPrice);

}
async function getPrice(fundID,decimalPlaces) {
    const fundPrice = await Price.model.find().where('fund', fundID).sort([['date', -1], ['_id', -1]]).limit(1).exec();
  //  console.log(fundPrice);
    const exists = fundPrice.length > 0;

    return ({
        price: exists && typeof fundPrice[0].price == 'number' ? fundPrice[0].price.toFixed(decimalPlaces) : 'N/A',
        bid: exists && typeof fundPrice[0].bid == 'number' ? fundPrice[0].bid.toFixed(2) : 'N/A',
        canc: exists && typeof fundPrice[0].canc == 'number' ? fundPrice[0].canc.toFixed(4) : 'N/A',
        yield: exists && fundPrice[0].yield ? fundPrice[0].yield : 'N/A',
        date: exists ? dateFormat(new Date(fundPrice[0].date), 'dd/mm/yyyy') : '00/00/0000',
    });
}

async function getDocument(table, tableID, docType, category) {
    let docQ,docCount;
    if (category != undefined) {
      docQ = await Document.model.find().where('type', docType).where('category', category).sort('-date').limit(1).exec();
      docCount = await Document.model.find().where('type', docType).where('category', category).sort('-date').limit(50).skip(1).exec();

    } else {
      docQ = await Document.model.find().where(table, tableID).where('type', docType).sort('-date').limit(1).exec();
      docCount = await Document.model.find().where(table, tableID).where('type', docType).sort('-date').limit(50).skip(1).exec();

    }
    if(docCount.length >1){
    //   console.log('ere we go:');
    // console.log(docType);
    docCount.map((oldDoc)=>{
    //   //console.log(oldDoc._id);
    //   //console.log(process.env.UPLOAD_LOCATION + '/documents/'+ oldDoc.name);
    //
    fs.stat(process.env.UPLOAD_LOCATION + '/documents/'+ oldDoc.name, function (err, stats) {
    //     //console.log(stats);//here we got all information of file in stats variable
    //
          if (err) {
            return console.error(err);
          }
          else{

    //        return fs.unlink(process.env.UPLOAD_LOCATION + '/documents/'+ oldDoc.name,function(err){
    //             if(err) return console.log(err);
    //             console.log('file deleted successfully');
    //             return Document.model.remove().where({"_id": oldDoc._id}).exec()
    //        });
         }
        });

    })

  }
    return docQ.length > 0 ? docQ[0].name : undefined;
}


async function getFundDocuments(fundID,range,sponsorID) {
    const keyInvestmentInformationDocument = await getDocument('fund', fundID, 'kiid');
    const factSheet = await getDocument('fund', fundID, 'factsheet');
    let prospectus = null;
    if(range.documentCategories.length){
    prospectus =await getDocument('fund', range.id, 'prospectus', range.documentCategories[0])
      if(typeof prospectus != 'undefined'){
        //i know this is not elegant, please don't look at me, I am ashamed
      }
      else{
        prospectus = await getDocument('fund', range.id, 'prospectus', range.documentCategories[1])
      }
    }else{
      prospectus = await getDocument('sponsor', sponsorID, 'prospectus')

    }
    const reportsAndAccounts = await getDocument('fund', range.id, 'reportsAndAccounts', range.documentCategories[1])

    let consolidatedFactsheet = null;
    if(range.documentCategories.length){
     consolidatedFactsheet = await getDocument('fund', range.id, 'consolidatedFactsheet', range.documentCategories[1])
      if(typeof consolidatedFactsheet != 'undefined'){
        //donothing
      }
      else{
        consolidatedFactsheet = await getDocument('fund', range.id, 'consolidatedFactsheet', range.documentCategories[0])
      }
    }
    else{
      consolidatedFactsheet  = await getDocument('sponsor', sponsorID, 'consolidatedFactsheet')
    }
      let reportsAndAccounts2 = null;
      if(range.documentCategories.length){
         reportsAndAccounts2 = await getDocument('fund', range.id, 'reportsAndAccounts2', range.documentCategories[1])
          if(typeof reportsAndAccounts2 != 'undefined'){
            //donothing
          }
          else{
            reportsAndAccounts2 = await getDocument('fund', range.id, 'reportsAndAccounts2', range.documentCategories[0])
          }
      }
      else{
        reportsAndAccounts2 = await getDocument('sponsor', sponsorID, 'reportsAndAccounts2')
      }


      let Supplement = null;
      if(range.documentCategories.length){
       Supplement = await getDocument('fund', range.id, 'Supplement', range.documentCategories[1])
        if(typeof Supplement != 'undefined'){
          //donothing
        }
        else{
          Supplement = await getDocument('fund', range.id, 'Supplement', range.documentCategories[0])
        }
      }
      else{
        Supplement  = await getDocument('sponsor', sponsorID, 'Supplement')
      }



    let EURApplicationForm = await getDocument('fund', range.id, 'EURApplicationForm', range.documentCategories[0])
      if(typeof EURApplicationForm != 'undefined'){
        //donothing
      }
      else{
        EURApplicationForm = await getDocument('fund', range.id, 'EURApplicationForm', range.documentCategories[1])
      }
    let USDApplicationForm = await getDocument('fund', range.id, 'USDApplicationForm', range.documentCategories[0])
      if(typeof USDApplicationForm != 'undefined'){
        //donothing
      }
      else{
        USDApplicationForm = await getDocument('fund', range.id, 'USDApplicationForm', range.documentCategories[1])
      }
    let GBPApplicationForm =await getDocument('fund', range.id, 'GBPApplicationForm', range.documentCategories[0])
      if(typeof GBPApplicationForm != 'undefined'){
        //donothing
      }
      else{
        GBPApplicationForm = await getDocument('fund', range.id, 'GBPApplicationForm', range.documentCategories[1])
      }

    let SupplementalProspectus = null;
    if(range.documentCategories.length){
    SupplementalProspectus = await getDocument('fund', range.id, 'SupplementalProspectus', range.documentCategories[0])
      if(typeof SupplementalProspectus != 'undefined'){
        //donothing
      }
      else{
        SupplementalProspectus = await getDocument('fund', range.id, 'SupplementalProspectus', range.documentCategories[1])
      }
    }
    else{
      SupplementalProspectus = await getDocument('sponsor', sponsorID, 'SupplementalProspectus')
    }


    const keyFeatures = await getDocument('sponsor', sponsorID, 'keyFeatures')
    const ISAKeyFeatures = await getDocument('sponsor', sponsorID, 'ISAKeyFeatures')

    const supplementaryInformationDocument = await getDocument('sponsor', sponsorID, 'supplementaryInformationDocument')
//    const reportsAndAccounts = await getDocument('sponsor', sponsorID, 'reportsAndAccounts'),

    const ISAApplicationForm = await getDocument('sponsor', sponsorID, 'ISAApplicationForm')
    const ISATransferForm = await getDocument('sponsor', sponsorID, 'ISATransferForm')
    const ISAConversionForm = await getDocument('sponsor', sponsorID, 'ISAConversionForm')
    const OEICApplicationForm = await getDocument('sponsor', sponsorID, 'OEICApplicationForm')

    const FacilitatedAdviserChargingDocuments = await getDocument('sponsor', sponsorID, 'FacilitatedAdviserChargingDocuments')



    const OEICCorpTrustsForm = await getDocument('sponsor', sponsorID, 'OEICCorpTrustsForm')
    const OEICSwitchForm = await getDocument('sponsor', sponsorID, 'OEICSwitchForm')
    const OEICConversionForm = await getDocument('sponsor', sponsorID, 'OEICConversionForm')
    const JISAApplicationForm = await getDocument('sponsor', sponsorID, 'JISAApplicationForm')
    const JISATermsAndConditions = await getDocument('sponsor', sponsorID, 'JISATermsAndConditions')
    const TermsAndConditions = await getDocument('sponsor', sponsorID, 'TermsAndConditions')

    const ISAApplicationFormUserGuide = await getDocument('sponsor', sponsorID, 'ISAApplicationFormUserGuide')
    const ISATransferApplicationFormUserGuide = await getDocument('sponsor', sponsorID, 'ISATransferApplicationFormUserGuide')
    const JuniorISAFormUserGuide = await getDocument('sponsor', sponsorID, 'JuniorISAFormUserGuide')
    const APSInvestmentUserGuide = await getDocument('sponsor', sponsorID, 'APSInvestmentUserGuide')
    const APSTransferUserGuide = await getDocument('sponsor', sponsorID, 'APSTransferUserGuide')
    //const Supplement = await getDocument('sponsor', sponsorID, 'Supplement')
    const OEICApplicationFormUserGuide = await getDocument('sponsor', sponsorID, 'OEICApplicationFormUserGuide')
    const CorpTrustsFormUserGuide = await getDocument('sponsor', sponsorID, 'CorpTrustsFormUserGuide')

    const WithdrawalForm = await getDocument('sponsor', sponsorID, 'WithdrawalForm')
    const WithdrawalFormUserGuide = await getDocument('sponsor', sponsorID, 'WithdrawalFormUserGuide')
    const TopUpAndRegularSaverAmendmentForm = await getDocument('sponsor', sponsorID, 'TopUpAndRegularSaverAmendmentForm')
    const RegularWithrawalForm = await getDocument('sponsor', sponsorID, 'RegularWithrawalForm')


    const TopUpAndRegularSaverAmendmentFormUserGuide = await getDocument('sponsor', sponsorID, 'TopUpAndRegularSaverAmendmentFormUserGuide')

    const ControllingPersonSelfCertificationForm = await getDocument('sponsor', sponsorID, 'ControllingPersonSelfCertificationForm')
    const EntitySelfCertificationForm = await getDocument('sponsor', sponsorID, 'EntitySelfCertificationForm')
    const IndividualSelfCertificationForm = await getDocument('sponsor', sponsorID, 'IndividualSelfCertificationForm')


    const TopupForm = await getDocument('sponsor', sponsorID, 'TopupForm')
    const APSForm= await getDocument('sponsor', sponsorID, 'APSForm')

    return {
      'keyInvestorInformationDocument': keyInvestmentInformationDocument,
      supplementaryInformationDocument,
      factSheet,
      prospectus,
      keyFeatures,
      ISAKeyFeatures,
      consolidatedFactsheet,
      reportsAndAccounts,
      reportsAndAccounts2,
      EURApplicationForm,
      USDApplicationForm,
      GBPApplicationForm,
      ISAApplicationForm,
      ISATransferForm,
      ISAConversionForm,
      JISAApplicationForm,
      JISATermsAndConditions,
      OEICApplicationForm,
      OEICCorpTrustsForm,
      OEICConversionForm,
      OEICSwitchForm,
      JISAApplicationForm,
      TermsAndConditions,
      Supplement,
      TopupForm,
      APSForm,
      SupplementalProspectus,
      ISAApplicationFormUserGuide,
      ISATransferApplicationFormUserGuide,
      JuniorISAFormUserGuide,
      APSInvestmentUserGuide,
      APSTransferUserGuide,
      FacilitatedAdviserChargingDocuments,
      CorpTrustsFormUserGuide,
      WithdrawalForm,
      WithdrawalFormUserGuide,
      TopUpAndRegularSaverAmendmentForm,
      RegularWithrawalForm,
      TopUpAndRegularSaverAmendmentFormUserGuide,
      ControllingPersonSelfCertificationForm,
      EntitySelfCertificationForm,
      IndividualSelfCertificationForm,
      OEICApplicationFormUserGuide
    };
}
async function getLitFunds(sponsor,docIWant) {
    const funds = await Funds.model.find().where('group', sponsor.id).sort('sortOrder').exec();

    const fundPromises = _.map(funds, async (fund) => {
      const link = await getDocument('fund', fund.id, docIWant);
        return {
            id:fund.id,
            name:fund.name,
            range:fund.range,
            link
        };
    });

    return await Promise.all(fundPromises)
}
async function getFunds(range,sponsor) {

  var decimalPlaces = 2;
  if(sponsor.IFSL || sponsor.DUBLIN){
    decimalPlaces = 4;
  }
  //console.log(sponsor);
  if(sponsor.International){
    decimalPlaces = 3;
  }
  var sponsorID = sponsor.id
    const rangeID = range.id
    const funds = await Funds.model.find().where('range', rangeID).sort('sortOrder').exec();
    const managersComments = await getManagersComments(rangeID)

    const fundPromises = _.map(funds, async (fund) => {
        const price = await getPrice(fund.id,parseInt(decimalPlaces));
        const className = await getClassName(fund.class);
        //const managerCommentary = await getManagersComments(fund.id, range, sponsorID)
        const documents = await getFundDocuments(fund.id,range,sponsorID);
        const team = await getTeam(fund.managementTeam);
        const time = dateFormat(new Date(sponsor.validationDate), 'HH:MM');
        return {
            id: fund.id,
            manager: fund.managementTeam,
            team: team,
            description: fund.summary,
            name: fund.name,
            dw: fund.dW,
            utoeic: fund.utOeic,
            accInc: fund.accInc,
            objective: fund.objective,
            class: className,
            time: time,
            price: price.price,
            canc: price.canc,
            yield: price.yield,
            bid: price.bid,
            date: price.date,
            ISIN: fund.ISIN,
            launchDate: fund.launchDate ? dateFormat(new Date(fund.launchDate), 'dd/mm/yyyy') : 'N/A',
            iaSector: fund.iaSector,
            MorningstarSector: fund.MorningstarSector,
            NisaQualifying: fund.NisaQualifying,
            sedolNumber: fund.sedolNumber,
            documents,
            managersComments
        };
    });

    return await Promise.all(fundPromises)
}

async function getFundRanges(sponsor) {
  var sponsorID = sponsor.id
    const fundRanges = await Fundrange.model.find().where('from', sponsorID).sort('sortOrder').exec();

    const ranges = _.map(fundRanges, async (range) => {
        //const documents = await getRangeDocuments(range.id);
        return {
            id: range.rangeID,
            name: range.name,
            category: range.marlboroughCategory,
            documentCategories: range.documentCategories,
            funds: _.keyBy(await getFunds(range,sponsor), 'sedolNumber'),
        }
    });

    return await Promise.all(ranges);
}


async function getTeam(teamID) {
  const managementTeam = await ManagementTeam.model.findOne(teamID).populate('people').exec();
  return managementTeam
}

async function getTheFunds(sponsorIAmLookingFor) {
    //const sponsor = await Sponsors.model.findOne({'name':sponsorIAmLookingFor}).exec();
        return   _.groupBy(_.keyBy(await getFundRanges(sponsorIAmLookingFor), 'name'),"category");

}
async function getliteratureLibrary(sponsor) {


    const sponsorName = sponsor.name;
//    const sponsor = await Sponsors.model.findOne({'name':sponsorName}).exec();

    const supplementaryInformationDocument = await getDocument('sponsor', sponsor.id, 'supplementaryInformationDocument', null);
    const keyFeatures = await getDocument('sponsor', sponsor, 'keyFeatures');

    const JISATermsAndConditions = await getDocument('sponsor', sponsor, 'JISATermsAndConditions', null);
    const TermsAndConditions = await getDocument('sponsor', sponsor, 'TermsAndConditions', null);
    const FacilitatedAdviserChargingDocuments = await getDocument('sponsor', sponsor, 'FacilitatedAdviserChargingDocuments',null);

      const ISAApplicationFormUserGuide = await getDocument('sponsor', sponsor, 'ISAApplicationFormUserGuide')
    const ISATransferApplicationFormUserGuide = await getDocument('sponsor', sponsor, 'ISATransferApplicationFormUserGuide')
    const JuniorISAFormUserGuide = await getDocument('sponsor', sponsor, 'JuniorISAFormUserGuide')
    const APSInvestmentUserGuide = await getDocument('sponsor', sponsor, 'APSInvestmentUserGuide')
    const APSTransferUserGuide = await getDocument('sponsor', sponsor, 'APSTransferUserGuide')
    const OEICApplicationFormUserGuide = await getDocument('sponsor', sponsor, 'OEICApplicationFormUserGuide',null);
    const CorpTrustsFormUserGuide = await getDocument('sponsor', sponsor, 'CorpTrustsFormUserGuide',null);

    const WithdrawalForm = await getDocument('sponsor', sponsor, 'WithdrawalForm',null);
    const WithdrawalFormUserGuide = await getDocument('sponsor', sponsor, 'WithdrawalFormUserGuide',null);
    const TopUpAndRegularSaverAmendmentForm = await getDocument('sponsor', sponsor, 'TopUpAndRegularSaverAmendmentForm',null);
    const TopUpAndRegularSaverAmendmentFormUserGuide = await getDocument('sponsor', sponsor, 'TopUpAndRegularSaverAmendmentFormUserGuide',null);
    const ControllingPersonSelfCertificationForm = await getDocument('sponsor', sponsor, 'ControllingPersonSelfCertificationForm')
    const EntitySelfCertificationForm = await getDocument('sponsor', sponsor, 'EntitySelfCertificationForm')
    const IndividualSelfCertificationForm = await getDocument('sponsor', sponsor, 'IndividualSelfCertificationForm')

    const RegularWithrawalForm = await getDocument('sponsor', sponsor, 'RegularWithrawalForm',null);


    let reportsAndAccounts2 = await getRangeDuexDocuments(sponsor.id,"reportsAndAccounts2",1)
      if(typeof reportsAndAccounts2[0].link != 'undefined'){
        //donothing
      }
      else{
        reportsAndAccounts2 = await getRangeDuexDocuments(sponsor.id,"reportsAndAccounts2",0)
      }
      let GBPApplicationForm = await getRangeDuexDocuments(sponsor.id,"GBPApplicationForm",1)
        if(typeof reportsAndAccounts2[0].link != 'undefined'){
          //donothing
        }
        else{
          GBPApplicationForm = await getRangeDuexDocuments(sponsor.id,"GBPApplicationForm",0)
        }

    //return await getRangeDuexDocuments(sponsor.id, "prospectus")
    if(sponsor.International){
      return  {
        "Application Forms": await getApplicationForms(sponsor),
        "EUR Application Forms": await getRangeDuexDocuments(sponsor.id, "EURApplicationForm",0),
        "USD Application Forms": await getRangeDuexDocuments(sponsor.id, "USDApplicationForm",0),
        "GBP Application Forms": GBPApplicationForm,
        "Fact Sheets": await getLitFunds(sponsor,"factsheet"),
        "Key Investor Information Document": await getLitFunds(sponsor,"kiid"),
        "Prospectus": await getRangeDuexDocuments(sponsor.id, "prospectus",0),
        "Reports And Accounts": reportsAndAccounts2,
        "Supplement": await getRangeDuexDocuments(sponsor.id,'Supplement',0),
        "FacilitatedAdviserChargingDocuments": await getDocument('sponsor', sponsor.id, 'FacilitatedAdviserChargingDocuments'),
        "OEICApplicationFormUserGuide": await getDocument('sponsor', sponsor.id, 'OEICApplicationFormUserGuide'),
        "CorpTrustsFormUserGuide": await getDocument('sponsor', sponsor.id, 'CorpTrustsFormUserGuide'),

        "WithdrawalForm": await getDocument('sponsor', sponsor.id, 'WithdrawalForm'),
        "WithdrawalFormUserGuide": await getDocument('sponsor', sponsor.id, 'WithdrawalFormUserGuide'),
        "TopUpAndRegularSaverAmendmentForm": await getDocument('sponsor', sponsor.id, 'TopUpAndRegularSaverAmendmentForm'),
        "TopUpAndRegularSaverAmendmentFormUserGuide": await getDocument('sponsor', sponsor.id, 'TopUpAndRegularSaverAmendmentFormUserGuide'),
         // "ControllingPersonSelfCertificationForm" : await getDocument('sponsor', sponsorID, 'ControllingPersonSelfCertificationForm'),
         // "EntitySelfCertificationForm" : await getDocument('sponsor', sponsorID, 'EntitySelfCertificationForm'),
         // "IndividualSelfCertificationForm" : await getDocument('sponsor', sponsorID, 'IndividualSelfCertificationForm'),
        "RegularWithrawalForm": await getDocument('sponsor', sponsor.id, 'RegularWithrawalForm'),


      }
    }else{

        return  {
          "Application Forms": await getApplicationForms(sponsor),
          "EUR Application Forms": await getRangeDuexDocuments(sponsor.id, "EURApplicationForm",0),
          "USD Application Forms": await getRangeDuexDocuments(sponsor.id, "USDApplicationForm",0),
          "GBP Application Forms": GBPApplicationForm,
          "Fact Sheets": await getLitFunds(sponsor,"factsheet"),
          "Key Investor Information Document": await getLitFunds(sponsor,"kiid"),
          "Supplement": await getRangeDuexDocuments(sponsor.id,'Supplement',0),
          "Facilitated Adviser Charging Documents":{
            sponsorName :{
              name:sponsorName,
              link:FacilitatedAdviserChargingDocuments
            }
          },
          "Prospectus": await getRangeDuexDocuments(sponsor.id, "prospectus",0),
          "Reports And Accounts": reportsAndAccounts2,
          "Supplementary Information Document":{
            sponsorName :{
              name:sponsorName,
              link:supplementaryInformationDocument
            }
          },
          "Terms And Conditions": {
            JISATermsAndConditions: {name:"JISA Terms And Conditions", link:JISATermsAndConditions},
            TermsAndConditions:{name: "Terms And Conditions", link: TermsAndConditions}
          },
          "User Guides":{
            OEICApplicationFormUserGuide:{name:"OEIC Application Form User Guide", link:OEICApplicationFormUserGuide},
            CorpTrustsFormUserGuide:{name:"Corp Trusts Form User Guide", link:CorpTrustsFormUserGuide},
            WithdrawalFormUserGuide:{name:"Withdrawal Form User Guide", link:WithdrawalFormUserGuide},
            TopUpAndRegularSaverAmendmentFormUserGuide:{name:"Top Up & Regular Saver Amendment Form User Guide", link:TopUpAndRegularSaverAmendmentFormUserGuide},
            ISAApplicationFormUserGuide:{name:"ISA Application Form User Guide", link:ISAApplicationFormUserGuide},
            ISATransferApplicationFormUserGuide:{name:"ISA Transfer Application Form User Guide",link:ISATransferApplicationFormUserGuide},
            JuniorISAFormUserGuide:{name:"Junior ISA Form User Guide",link:JuniorISAFormUserGuide},
            APSInvestmentUserGuide:{name:"APS Investment User Guide",link:APSInvestmentUserGuide},
            APSTransferUserGuide:{name:"APS Transfer User Guide",link:APSTransferUserGuide}
          },
          "Tax Residency Self-certification":{
             ControllingPersonSelfCertificationForm : {name:"Controlling Person",link:ControllingPersonSelfCertificationForm},
             EntitySelfCertificationForm : {name:"Entity",link:EntitySelfCertificationForm},
             IndividualSelfCertificationForm :{name:"Individual",link:IndividualSelfCertificationForm}
          }
        }
      }
        //await getFundRanges(sponsor.id);

}
async function getRangeDuexDocuments(sponsorID,whatILookFor,whereToLook){

//  return await getDocument('fund', range.id, whatILookFor, range.documentCategories[1])
  const fundRanges = await Fundrange.model.find().where('from', sponsorID).sort('sortOrder').exec();
  //return fundRanges
  const ranges = _.map(fundRanges, async (range) => {
      //return getRangeDocuments(range);
//Acumen Strategic Portfolio
  let theLink = null;

//if(whatILookFor === "Supplement"){

  if(range.documentCategories.length){
   theLink = await getDocument('fund', range.id, whatILookFor, range.documentCategories[1])
    if(typeof theLink != 'undefined'){
      //donothing
    }
    else{
      theLink = await getDocument('fund', range.id, whatILookFor, range.documentCategories[0])
    }
  }
  else{
    theLink  = await getDocument('sponsor', sponsorID, whatILookFor)
  }
// }else{
//   if (range.documentCategories.length){
//
//     theLink = await getDocument('fund', range.id, whatILookFor, range.documentCategories[whereToLook])
//     //getDocument('fund', range.id, 'prospectus', range.documentCategories[0])
//     // if(theLink){
//     // console.log("CAT DOC________________________");
//     // console.log(theLink);}
//   }
//   else {
//     theLink = await getDocument('sponsor', sponsorID, whatILookFor)
//   }
// }



      return {
        range: range.id,
        link: theLink,//await getDocument('fund', range.id, range.documentCategories[whatILookFor]),
        name: range.name
      };

  });
  return await Promise.all(ranges)

}
async function getApplicationForms(sponsor) {

//   ISA Application Form
const ISAApplicationForm = await getDocument('sponsor', sponsor.id, 'ISAApplicationForm');
const ISATransferForm = await getDocument('sponsor', sponsor.id, 'ISATransferForm');
const ISAConversionForm = await getDocument('sponsor', sponsor.id, 'ISAConversionForm');
// OEIC Application Form
const OEICApplicationForm = await getDocument('sponsor', sponsor.id, 'OEICApplicationForm');


const OEICCorpTrustsForm = await getDocument('sponsor', sponsor.id, 'OEICCorpTrustsForm');
// OEIC Switch Form
const OEICSwitchForm = await getDocument('sponsor', sponsor.id, 'OEICSwitchForm');
// OEIC Conversion Form

const OEICConversionForm = await getDocument('sponsor', sponsor.id, 'OEICConversionForm');
// JISA Application Form
const JISAApplicationForm = await getDocument('sponsor', sponsor.id, 'JISAApplicationForm');

const TopupForm = await getDocument('sponsor', sponsor.id, 'TopupForm');
// APS Form
const APSForm = await getDocument('sponsor', sponsor.id, 'APSForm');
const WithdrawalForm = await getDocument('sponsor', sponsor.id, 'WithdrawalForm');
const TopUpAndRegularSaverAmendmentForm = await getDocument('sponsor', sponsor.id, 'TopUpAndRegularSaverAmendmentForm');
const RegularWithrawalForm = await getDocument('sponsor', sponsor.id, 'RegularWithrawalForm');


//EUR Application Form
let EURApplicationForm = await getRangeDuexDocuments(sponsor.id, 'EURApplicationForm', 0)
  if(typeof EURApplicationForm != 'undefined'){
    //donothing

  }
  else{
    EURApplicationForm = await getDocument(sponsor.id, 'EURApplicationForm', 1)
  }

  return {
    ISAApplicationForm:{name:"ISA Application Form",link:ISAApplicationForm},
    ISATransferForm:{name:"ISA Transfer Form",link:ISATransferForm},
    ISAConversionForm:{name:"ISA Conversion Form",link:ISAConversionForm},
    OEICApplicationForm:{name:"OEIC Application Form",link:OEICApplicationForm},
    OEICCorpTrustsForm:{name:"OEIC Corp Trusts Form",link:OEICCorpTrustsForm},
    OEICSwitchForm:{name:"OEIC Switch Form",link:OEICSwitchForm},
    OEICConversionForm:{name:"OEIC Conversion Form",link:OEICConversionForm},
    JISAApplicationForm:{name:"JISA Application Form",link:JISAApplicationForm},
    TopupForm:{name:"Topup Form",link:TopupForm},
    APSForm:{name:"APS Form",link:APSForm},
    WithdrawalForm:{name:"Withdrawal Form",link:WithdrawalForm},
    TopUpAndRegularSaverAmendmentForm:{name:"Top Up & Regular Saver Amendment Form",link:TopUpAndRegularSaverAmendmentForm},
    RegularWithrawalForm:{name:"Regular Withrawal Form",link:RegularWithrawalForm}
  }
}
async function getSponsors(req) {
    let filterVar = req.params.sponsorName;
    if (filterVar !== "Marlborough" && filterVar !==  "International"){
        filterVar = filterVar.toUpperCase();
    }
    // if(filterVar == "ifsl"){
    //   filterVar = "IFSL";
    // }
    //const sponsors = await Sponsors.model.find({'name':}).sort('sortOrder').exec();
    //console.log(filterVar);
    var qName = filterVar;
    var qValue = true;
    var query = {};
    query[qName] = qValue;
    //collection.findOne(query, function (err, item) { ... });
    const sponsors = await Sponsors.model.find(query).exec();

    const sponsorPromises = _.map(sponsors, async (sponsor) => {
        return {
             name: sponsor.name,
             marlborough: sponsor.Marlborough,
             mfm: sponsor.MFM,
             dublin: sponsor.DUBLIN,
             IFSL: sponsor.IFSL,
             description: sponsor.description,
             logo: sponsor.logo.filename,
             banner: sponsor.ifslBannerImage.filename,
             color: sponsor.color,
             valuationPoint: dateFormat(new Date(sponsor.validationDate), 'HH:MM'),
             fundRanges: await getTheFunds(sponsor),
             literature: await getliteratureLibrary(sponsor)
        };
    });

    const sponsorsParsed = await Promise.all(sponsorPromises);


    return {
        sponsors: _(sponsorsParsed).keyBy('name').value(),
    };
}


exports = module.exports = async function (req, res) {
    try {
        res.json(await getSponsors(req));
    } catch (err) {
        console.log(err);
    }
};
