var keystone = require('keystone');
var Ranges = keystone.list('FundRange');
var ManagerCommentary= keystone.list('ManagerCommentary')
import R from 'ramda';
var fs = require('fs');

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;
	Ranges.model.find().exec().then(function(result){
		locals.ranges = result;
		view.render('managersCommentsForm');
	}, function(err){
		console.log(err);
		req.flash('error','Error saving commment')
		next();
	});

	function fileExists(path) {
	  try {
	    let stats = fs.lstatSync(path);
	    if (stats.isFile()) {
	        return true;
	    }
	    return false;
	  } catch (e) {
	    return false;
	  }
	  return false;
	}
	function copyFile(source, target) {
	    return new Promise(function(resolve, reject) {
	        var rd = fs.createReadStream(source);
	        rd.on('error', rejectCleanup);
	        var wr = fs.createWriteStream(target);
	        wr.on('error', rejectCleanup);
	        function rejectCleanup(err) {
	            rd.destroy();
	            wr.end();
	            reject(err);
	        }
	        wr.on('finish', resolve);
	        rd.pipe(wr);
	    });
	}
	view.on('post', function(next){
		if(req.body.title && req.body.content && req.files.file){
				try {
						const files = R.when(R.complement(R.isArrayLike), R.of)(req.files.file);

						const results = R.map(file => new Promise((resolve, reject) => {

								const path = file.path;

								const time = (new Date()).valueOf().toString();
		            const originalname = file.originalname;
		            const newLoc = process.env.UPLOAD_LOCATION + '/comments/' + time + originalname;
		            copyFile(file.path, newLoc).then(() => {
		                const newComment = new ManagerCommentary.model({
												title: req.body.title,
												content: req.body.content,
												range: req.body.range,
												pdf: time + originalname
		                });
		                newComment.save(function(err) {
		                    if (err) return reject(err);
												
		                    return resolve();
		                });
								});
						}))(files);
				} catch (e) {
					req.flash('error',"There was a problem uploadinf the PDF")
						console.log(e);
				}
		next();
	}
	else{
		req.flash('error',"Please fill in all the fields")
		next()
	}
  });


};
