import dateFormat from 'dateformat';
import _ from 'lodash';
import R from 'ramda';

var keystone = require('keystone');
var Sponsors = keystone.list('FundGroup');
var Funds = keystone.list('Fund');
var Fundrange = keystone.list('FundRange');
var FundClass = keystone.list('FundClass');
var Document = keystone.list('Document');


async function getDocument(table, tableID, docType, category) {
    let docQ;
    if (category != undefined) {
      docQ = await Document.model.find().where('type', docType).where('category', category).sort('-date').limit(1).exec();
    } else {
      docQ = await Document.model.find().where(table, tableID).where('type', docType).sort('-date').limit(1).exec();
    }
    return docQ.length > 0 ? docQ[0].name : undefined;
}





async function getRangeForms(sponsor,docIWant) {


    // let prospectus = await getDocument('fund', range.id, 'prospectus', range.documentCategories[0])
    //   if(typeof prospectus != 'undefined'){
    //     //i know this is not elegant, please don't look at me, I am ashamed
    //   }
    //   else{
    //     prospectus = await getDocument('fund', range.id, 'prospectus', range.documentCategories[1])
    //   }
    //
    // const reportsAndAccounts = await getDocument('fund', range.id, 'reportsAndAccounts', range.documentCategories[1])
    // let reportsAndAccounts2 = await getDocument('fund', range.id, 'reportsAndAccounts2', range.documentCategories[1])
    //   if(typeof reportsAndAccounts2 != 'undefined'){
    //     //donothing
    //   }
    //   else{
    //     reportsAndAccounts2 = await getDocument('fund', range.id, 'reportsAndAccounts2', range.documentCategories[0])
    //   }
    const fundRanges = await Fundrange.model.find().where('from', sponsor.id).sort('sortOrder').exec();

    const ranges = _.map(fundRanges, async (range) => {
        //return getRangeDocuments(range);
        return {
          prospectus: await getRangeDuexDocuments(range, "prospectus",0),
          reportsAndAccounts2: await getRangeDuexDocuments(range,"reportsAndAccounts2",1)
        };

    });


    let EURApplicationForm = await getDocument('fund', range.id, 'EURApplicationForm', range.documentCategories[0])
      if(typeof EURApplicationForm != 'undefined'){
        //donothing

      }
      else{
        EURApplicationForm = await getDocument('fund', range.id, 'EURApplicationForm', range.documentCategories[1])
      }
    let USDApplicationForm = await getDocument('fund', range.id, 'USDApplicationForm', range.documentCategories[0])
      if(typeof USDApplicationForm != 'undefined'){
        //donothing
      }
      else{
        USDApplicationForm = await getDocument('fund', range.id, 'USDApplicationForm', range.documentCategories[1])
      }
    let GBPApplicationForm =await getDocument('fund', range.id, 'GBPApplicationForm', range.documentCategories[0])
      if(typeof GBPApplicationForm != 'undefined'){
        //donothing
      }
      else{
        GBPApplicationForm = await getDocument('fund', range.id, 'GBPApplicationForm', range.documentCategories[1])
      }

    const keyFeatures = await getDocument('sponsor', sponsorID, 'keyFeatures')
    const supplementaryInformationDocument = await getDocument('sponsor', sponsorID, 'supplementaryInformationDocument')
//    const reportsAndAccounts = await getDocument('sponsor', sponsorID, 'reportsAndAccounts'),

    const ISAApplicationForm = await getDocument('sponsor', sponsorID, 'ISAApplicationForm')
    const ISATransferForm = await getDocument('sponsor', sponsorID, 'ISATransferForm')
    const ISAConversionForm = await getDocument('sponsor', sponsorID, 'ISAConversionForm')
    const OEICApplicationForm = await getDocument('sponsor', sponsorID, 'OEICApplicationForm')

    const OEICCorpTrustsForm = await getDocument('sponsor', sponsorID, 'OEICCorpTrustsForm')
    const OEICSwitchForm = await getDocument('sponsor', sponsorID, 'OEICSwitchForm')
    const OEICConversionForm = await getDocument('sponsor', sponsorID, 'OEICConversionForm')
    const JISAApplicationForm = await getDocument('sponsor', sponsorID, 'JISAApplicationForm')
    const JISATermsAndConditions = await getDocument('sponsor', sponsorID, 'JISATermsAndConditions')
    const ISATermsAndConditions = await getDocument('sponsor', sponsorID, 'ISATermsAndConditions')

    const TopupForm = await getDocument('sponsor', sponsorID, 'TopupForm')
    const APSForm= await getDocument('sponsor', sponsorID, 'APSForm')

    return {
      keyInvestmentInformationDocument,
      supplementaryInformationDocument,
      factSheet,
      prospectus,
      keyFeatures,
      reportsAndAccounts,
      reportsAndAccounts2,
      EURApplicationForm,
      USDApplicationForm,
      GBPApplicationForm,
      ISAApplicationForm,
      ISATransferForm,
      ISAConversionForm,
      JISAApplicationForm,
      JISATermsAndConditions,
      OEICApplicationForm,
      OEICCorpTrustsForm,
      OEICConversionForm,
      OEICSwitchForm,
      JISAApplicationForm,
      ISATermsAndConditions,
      JISATermsAndConditions,
      TopupForm,
      APSForm
    };
}

async function getFunds(sponsor,docIWant) {
    const funds = await Funds.model.find().where('group', sponsor.id).sort('sortOrder').exec();

    const fundPromises = _.map(funds, async (fund) => {
      const link = await getDocument('fund', fund.id, docIWant);
        return {
            id:fund.id,
            name:fund.name,
            range:fund.range,
            link
        };
    });

    return await Promise.all(fundPromises)
}

async function getFundRanges(sponsorID) {
    const fundRanges = await Fundrange.model.find().where('from', sponsorID).sort('sortOrder').exec();

    const ranges = _.map(fundRanges, async (range) => {
        //return getRangeDocuments(range);
        return {
          prospectus: await getRangeDuexDocuments(range, "prospectus",0),
          reportsAndAccounts2: await getRangeDuexDocuments(range,"reportsAndAccounts2",1)
        };

    });

  //  return await Promise.all(ranges);
}

async function getAllCategories(sponsor) {
    return await Document.model.find().where('sponsor', sponsor).distinct('category').exec();
}

async function getSponsors(req,res) {
    const sponsorName = req.params.sponsorName;
    const sponsor = await Sponsors.model.findOne({'name':sponsorName}).exec();

    const supplementaryInformationDocument = await getDocument('sponsor', sponsor.id, 'supplementaryInformationDocument', "");
    const keyFeatures = await getDocument('sponsor', sponsor, 'keyFeatures');

    const JISATermsAndConditions = await getDocument('sponsor', sponsor, 'JISATermsAndConditions', "");
    const ISATermsAndConditions = await getDocument('sponsor', sponsor, 'ISATermsAndConditions', "");

    let reportsAndAccounts2 = await getRangeDuexDocuments(sponsor.id,"reportsAndAccounts2",1)
      if(typeof reportsAndAccounts2[0].link != 'undefined'){
        //donothing
      }
      else{
        reportsAndAccounts2 = await getRangeDuexDocuments(sponsor.id,"reportsAndAccounts2",0)
      }
    //return await getRangeDuexDocuments(sponsor.id, "prospectus")
        return  {
          "Application Forms": await getApplicationForms(sponsor),
          "EUR Application Forms": await getRangeDuexDocuments(sponsor.id, "EURApplicationForm",0),
          "USD Application Forms": await getRangeDuexDocuments(sponsor.id, "USDApplicationForm",0),
          "GBP Application Forms": await getRangeDuexDocuments(sponsor.id, "GBPApplicationForm",0),
          "Fact Sheet": await getFunds(sponsor,"factsheet"),
          "Key Investment Information Document": await getFunds(sponsor,"kiid"),
          // "Key Features": {
          //   sponsorName :{
          //     name:sponsorName,link:keyFeatures
          //   }
          // },

          "Prospectus": await getRangeDuexDocuments(sponsor.id, "prospectus",0),
          "Reports And Accounts": reportsAndAccounts2,
          "Supplementary Information Document":{
            sponsorName :{
              name:sponsorName,
              link:supplementaryInformationDocument
            }
          },
          "Terms And Conditions": {
            JISATermsAndConditions: {name:"JISA Terms And Conditions", link:JISATermsAndConditions},
            ISATermsAndConditions:{name: "ISA Terms And Conditions", link: ISATermsAndConditions}
          }
        }
        //await getFundRanges(sponsor.id);

}
async function getApplicationForms(sponsor) {

//   ISA Application Form
const ISAApplicationForm = await getDocument('sponsor', sponsor.id, 'ISAApplicationForm');
const ISATransferForm = await getDocument('sponsor', sponsor.id, 'ISATransferForm');
const ISAConversionForm = await getDocument('sponsor', sponsor.id, 'ISAConversionForm');
// OEIC Application Form
const OEICApplicationForm = await getDocument('sponsor', sponsor.id, 'OEICApplicationForm');
const OEICCorpTrustsForm = await getDocument('sponsor', sponsor.id, 'OEICCorpTrustsForm');
// OEIC Switch Form
const OEICSwitchForm = await getDocument('sponsor', sponsor.id, 'OEICSwitchForm');
// OEIC Conversion Form

const OEICConversionForm = await getDocument('sponsor', sponsor.id, 'OEICConversionForm');
// JISA Application Form
const JISAApplicationForm = await getDocument('sponsor', sponsor.id, 'JISAApplicationForm');

const TopupForm = await getDocument('sponsor', sponsor.id, 'TopupForm');
// APS Form
const APSForm = await getDocument('sponsor', sponsor.id, 'APSForm');

//EUR Application Form
let EURApplicationForm = await getRangeDuexDocuments(sponsor.id, 'EURApplicationForm', 0)
  if(typeof EURApplicationForm != 'undefined'){
    //donothing

  }
  else{
    EURApplicationForm = await getDocument(sponsor.id, 'EURApplicationForm', 1)
  }

  return {
    ISAApplicationForm:{name:"ISA Application Form",link:ISAApplicationForm},
    ISATransferForm:{name:"ISA Transfer Form",link:ISATransferForm},
    ISAConversionForm:{name:"ISA Conversion Form",link:ISAConversionForm},
    OEICApplicationForm:{name:"OEIC Application Form",link:OEICApplicationForm},
    OEICCorpTrustsForm:{name:"OEIC Corp Trusts Form",link:OEICCorpTrustsForm},
    OEICSwitchForm:{name:"OEIC Switch Form",link:OEICSwitchForm},
    OEICConversionForm:{name:"OEIC Conversion Form",link:OEICConversionForm},
    JISAApplicationForm:{name:"JISA Application Form",link:JISAApplicationForm},
    TopupForm:{name:"Topup Form",link:TopupForm},
    APSForm:{name:"APS Form",link:APSForm}
  }
}
async function getRangeDuexDocuments(sponsorID,whatILookFor,whereToLook){
//  return await getDocument('fund', range.id, whatILookFor, range.documentCategories[1])
  const fundRanges = await Fundrange.model.find().where('from', sponsorID).sort('sortOrder').exec();
  //return fundRanges
  const ranges = _.map(fundRanges, async (range) => {
      //return getRangeDocuments(range);

      return {
        range: range.id,
        link: await getDocument('fund', range.id, whatILookFor, range.documentCategories[whereToLook]),
        name: range.name
      };

  });
  return await Promise.all(ranges)

}

async function getSponsorDocuments(sponsor) {


        return {

        };

    const documents = await Promise.all(docs);

    const final = R.pipe(
        (R.map(R.filter(R.identity))),
        R.mergeAll
    )(documents);
    return final;
}

exports = module.exports = async function (req, res) {
    try {
      res.json(await getSponsors(req));
    } catch (err) {
        console.log(err);
    }
};
