import dateFormat from 'dateformat';
import _ from 'lodash';

var keystone = require('keystone');
var Funds = keystone.list('Fund');
var FundRange = keystone.list('FundRange');
var Document = keystone.list('Document');


const fundTypes = [
  'reportsAndAccounts',
  'reportsAndAccounts2',
];


async function hoistFunds(range) {
    const funds = await Funds.model.find({}).exec();
    await _.map(funds, async (fund) => {
        const promises = _.map(fundTypes, docType => new Promise( (resolve, reject) => {
            if (fund[docType] !== {}) {
                const newDoc = new Document.model({
                    name: fund[docType].filename,
                    type: docType,
                    date: Date.now(),
                    fund: undefined,
                    fundRange: fund.range,
                });
                newDoc.save(function(err) {
                    if (err) return reject(err);
                    return resolve();
                });
            }
            return resolve();
        }));
        await Promise.all(promises);
    });
}

const hoist = async () => {
    await hoistFunds();
    return {done: true};
}


exports = module.exports = async function (req, res) {
    try {
        res.json(await hoist());
    } catch (err) {
        console.log(err);
    }
};
