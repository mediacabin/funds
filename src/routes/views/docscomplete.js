import R from 'ramda';

var keystone = require('keystone');
var csv = require('csv');
var fs = require('fs');
var _ = require('lodash');
var moment = require('moment');
var path = require('path');

var Document = keystone.list('Document');

function copyFile(source, target) {
    return new Promise(function(resolve, reject) {
        var rd = fs.createReadStream(source);
        rd.on('error', rejectCleanup);
        var wr = fs.createWriteStream(target);
        wr.on('error', rejectCleanup);
        function rejectCleanup(err) {
            rd.destroy();
            wr.end();
            reject(err);
        }
        wr.on('finish', resolve);
        rd.pipe(wr);
    });
}

exports = module.exports = async function(req, res) {
    var view = new keystone.View(req, res);
    var locals = res.locals;

    try {
        const files = JSON.parse(req.body.data);

        const results = R.map(file => new Promise((resolve, reject) => {
            const time = (new Date()).valueOf().toString();
            const base = path.basename(file.path);
            const newLoc = process.env.UPLOAD_LOCATION + '/documents/' + file.original;
            copyFile(file.path, newLoc).then(() => {

                const newDoc = new Document.model({
                    name: file.original,
                    type: file.type,
                    date: Date.now(),
                    fund: file.fund,
                    sponsor: file.sponsor,
                    category: file.category,
                });
                // console.log('file');
                // console.log(file);
                // console.log('base');
                // console.log(base);
                // console.log('newLoc');
                // console.log(newLoc);
                // console.log('File PAth');
                // console.log(file.path);
                // console.log('NewDoc');
                // console.log(newDoc);

                newDoc.save(function(err) {
                    if (err) return reject(err);

                     fs.unlink(file.path,function(err){
                          if(err) return console.log(err);
                          console.log('file deleted successfully');
                          return resolve();
                     });

                });
            });
        }))(files);

        Promise.all(results)
            .then(() => view.render('docscomplete'));

    } catch (e) {
        console.log(e);
    }
};
