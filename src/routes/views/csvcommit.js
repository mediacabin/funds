var keystone = require('keystone');
var csv = require('csv');
var fs = require('fs');
var _ = require('lodash');
var moment = require('moment');

var Funds = keystone.list('Fund');

var Price = keystone.list('Price');

exports = module.exports = async function(req, res) {
	const data = req.body.data;
	const parsedData = JSON.parse(data);

	let promises = [];
	_.map(parsedData, (row) => {
		if (row.fail) return;
		const newPrice = new Price.model({
			price: row.price,
			date: new Date(row.date),
			fund: row.fund,
			bid: row.bid,
			canc: row.canc,
			yield: row.yield,
		});
		promises.push(new Promise((resolve) => {
			newPrice.save(function(err) {
				return resolve();
			});
		}));
	});

	Promise.all(promises).then()

	var view = new keystone.View(req, res);
	var locals = res.locals;
	Promise.all(promises).then(() => {
		view.render('csvcommit');
	});
};
