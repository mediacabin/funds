var keystone = require('keystone');
var csv = require('csv');
var fs = require('fs');
var _ = require('lodash');
var moment = require('moment');
var R = require('ramda');

var Funds = keystone.list('Fund');

var Price = keystone.list('Price');

const parseDate = (date) => {
	var m = date.match(/^(\d{1,2})\/(\d{1,2})\/(\d{1,2})$/);
	if (m) {
		return new Date(Date.UTC('20'+m[3], m[2]-1, m[1]))
	}
	m = date.match(/^(\d{1,2})\/(\d{1,2})\/(\d{1,4})$/);
	return new Date(Date.UTC(m[3], m[2]-1, m[1]));
}


function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

async function parseRow(row, index) {
		if (row["SEDOL No."] == null) {
			return {fail: true, message: 'Failed To Parse Row '+index};
		}

		let fundID = null;
		const matching = await Funds.model.find().where('sedolNumber', row["SEDOL No."]).exec();
		if (matching[0]) {
			fundID = matching[0]._id;
		} else {
			return {fail: true, message: 'SEDOL number of '+row["SEDOL No."]+' dosent exist in database'};
		}

		return {
			fail: false,
			price: row['Offer/NAV'],
			bid: isNumeric(row['Bid']) ? row['Bid'] : undefined,
			canc: isNumeric(row['Canc.']) ? row['Canc.'] : undefined,
			yield: row['Yield'],
			date: parseDate(row['Val\'n Date']),
			fund: fundID,
			sedol: row["SEDOL No."],
			name: matching[0].name,
		};
}

exports = module.exports = async function(req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;

  const files = R.when(R.complement(R.isArrayLike), R.of)(req.files.file);

  var filesData = _.map(files, (file) => new Promise((resolve, reject) => {
		return fs.readFile(file.path, {encoding: 'utf-8'}, (err, data) => {if (err) reject(err); resolve(data);});
	}).catch((e) => {console.log(e); res.status(500)}));
  filesData = await Promise.all(filesData);

	var filesParsed = _.map(filesData, (fileData) => new Promise((resolve, reject) => {
		csv.parse(fileData, {columns: true}, (err, data) => {if (err) reject(err); resolve(data)});
	}).catch((e) => {console.log(e); res.status(500)}));
  filesParsed = await Promise.all(filesParsed);

  const rows = _.map(filesParsed, parsedFile => _.map(parsedFile, parseRow));

	Promise.all(_.flatten(rows)).then((v) => {
		locals.results = v;
		view.render('csvconfirm');
	});
};
