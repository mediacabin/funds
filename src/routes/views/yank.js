import R from 'ramda';

var keystone = require('keystone');
var Funds = keystone.list('Fund');
var FundGroup = keystone.list('FundGroup');
var FundRange = keystone.list('FundRange');
var Document = keystone.list('Document');




async function hoistFunds(range) {
    const documents = await Document.model.find().where('fundRange').ne(null).exec();
    const s = documents.map(async (doc) => {
        const fundRange = await FundRange.model.findOne().where('_id', doc.fundRange).exec();
        const sponsor = await FundGroup.model.findOne().where('_id', fundRange.from).exec();
        doc.fundRange = null;
        doc.sponsor = sponsor._id;
        doc.category = '';
        doc.save();
        console.log(3, sponsor);
    })
}

const hoist = async () => {
    await hoistFunds();
    return {done: true};
}


exports = module.exports = async function (req, res) {
    try {
        res.json(await hoist());
    } catch (err) {
        console.log(err);
    }
};
