import dateFormat from 'dateformat';
import _ from 'lodash';
import R from 'ramda';

var keystone = require('keystone');
var Sponsors = keystone.list('FundGroup');
var Funds = keystone.list('Fund');
var Fundrange = keystone.list('FundRange');
var FundClass = keystone.list('FundClass');
var Manager = keystone.list('Manager');
var Price = keystone.list('Price');
var Manager = keystone.list('Manager');
var Document = keystone.list('Document');
var ManagementTeam = keystone.list('ManagementTeam');

async function getClassName(classID) {
    const fundClass = await FundClass.model.findById(classID).exec();
    return fundClass.name;
}

async function getPrice(fundID) {
    const fundPrice = await Price.model.find().where('fund', fundID).sort('-date').limit(1).exec();
    const exists = fundPrice.length > 0;
    return ({
        price: exists && typeof fundPrice[0].price == 'number' ? fundPrice[0].price.toFixed(4) : 'N/A',
        date: exists ? dateFormat(new Date(fundPrice[0].date), 'dd/mm/yyyy') : '00/00/0000',
    });
}

async function getDocument(table, tableID, docType, category) {
    let docQ;
    if (category != undefined) {
      docQ = await Document.model.find().where(table, tableID).where('type', docType).where('category', category).sort('-date').limit(1).exec();
    } else {
      docQ = await Document.model.find().where(table, tableID).where('type', docType).sort('-date').limit(1).exec();
    }
    return docQ.length > 0 ? docQ[0].name : undefined;
}

async function getAllCategories(sponsor) {
    return await Document.model.find().where('sponsor', sponsor).distinct('category').exec();
}

async function getSponsorDocuments(sponsor) {
    const allCats = await getAllCategories(sponsor);
    const categories = R.unless(R.contains(''), R.concat(['']), allCats);
    const docs = categories.map(async (cat) => {

        const keyFeatures = await getDocument('sponsor', sponsor, 'keyFeatures', cat);
        const supplementaryInformationDocument = await getDocument('sponsor', sponsor, 'supplementaryInformationDocument', cat);
        const reportsAndAccounts = await getDocument('sponsor', sponsor, 'reportsAndAccounts', cat);
        const reportsAndAccounts2 = await getDocument('sponsor', sponsor, 'reportsAndAccounts2', cat);
        const prospectus = await getDocument('sponsor', sponsor, 'prospectus', cat);
        const ISAApplicationForm = await getDocument('sponsor', sponsor, 'ISAApplicationForm', cat);
        const GBPApplicationForm = await getDocument('sponsor', sponsor, 'GBPApplicationForm', cat);
        const USDApplicationForm = await getDocument('sponsor', sponsor, 'USDApplicationForm', cat);
        const EURApplicationForm = await getDocument('sponsor', sponsor, 'EURApplicationForm', cat);
        const ISATransferForm = await getDocument('sponsor', sponsor, 'ISATransferForm', cat);
        const ISAConversionForm = await getDocument('sponsor', sponsor, 'ISAConversionForm', cat);
        const OEICApplicationForm = await getDocument('sponsor', sponsor, 'OEICApplicationForm', cat);
        const OEICCorpTrustsForm = await getDocument('sponsor', sponsor, 'OEICCorpTrustsForm', cat);
        const OEICSwitchForm = await getDocument('sponsor', sponsor, 'OEICSwitchForm', cat);
        const OEICConversionForm = await getDocument('sponsor', sponsor, 'OEICConversionForm', cat);
        const JISAApplicationForm = await getDocument('sponsor', sponsor, 'JISAApplicationForm', cat);
        const JISATermsAndConditions = await getDocument('sponsor', sponsor, 'JISATermsAndConditions', cat);
        const ISATermsAndConditions = await getDocument('sponsor', sponsor, 'ISATermsAndConditions', cat);
        const TopupForm = await getDocument('sponsor', sponsor, 'TopupForm', cat);
        const APSForm = await getDocument('sponsor', sponsor, 'APSForm', cat);

        return {
            [cat]: {
                keyFeatures,
                supplementaryInformationDocument,
                reportsAndAccounts,
                reportsAndAccounts2,
                prospectus,
                ISAApplicationForm,
                EURApplicationForm,
                USDApplicationForm,
                GBPApplicationForm,
                ISATransferForm,
                ISAConversionForm,
                OEICApplicationForm,
                OEICCorpTrustsForm,
                OEICSwitchForm,
                OEICConversionForm,
                JISAApplicationForm,
                JISATermsAndConditions,
                ISATermsAndConditions,
                TopupForm,
                APSForm
            }
        };
    });
    const documents = await Promise.all(docs);

    const final = R.pipe(
        (R.map(R.filter(R.identity))),
        R.mergeAll
    )(documents);
    return final;
}

async function getFundDocuments(fundID) {
    const kiid = await getDocument('fund', fundID, 'kiid');
    const factSheet = await getDocument('fund', fundID, 'factsheet');
    return {kiid, factSheet};
}


async function getRangeDocuments(rangeID) {
    const keyFeatures = await getDocument('fundRange', rangeID, 'keyFeatures');
    const supplementaryInformationDocument = await getDocument('fundRange', rangeID, 'supplementaryInformationDocument');
    const reportsAndAccounts = await getDocument('fundRange', rangeID, 'reportsAndAccounts');
    const reportsAndAccounts2 = await getDocument('fundRange', rangeID, 'reportsAndAccounts2');
    const prospectus = await getDocument('fundRange', rangeID, 'prospectus');
    const ISAApplicationForm = await getDocument('fundRange', rangeID, 'ISAApplicationForm');
    const GBPApplicationForm = await getDocument('fundRange', rangeID, 'GBPApplicationForm');
    const USDApplicationForm = await getDocument('fundRange', rangeID, 'USDApplicationForm');
    const EURApplicationForm = await getDocument('fundRange', rangeID, 'EURApplicationForm');
    const ISATransferForm = await getDocument('fundRange', rangeID, 'ISATransferForm');
    const ISAConversionForm = await getDocument('fundRange', rangeID, 'ISAConversionForm');
    const OEICApplicationForm = await getDocument('fundRange', rangeID, 'OEICApplicationForm');
    const OEICCorpTrustsForm = await getDocument('fundRange', rangeID, 'OEICCorpTrustsForm');
    const OEICSwitchForm = await getDocument('fundRange', rangeID, 'OEICSwitchForm');
    const JISAApplicationForm = await getDocument('fundRange', rangeID, 'JISAApplicationForm');
    const TopupForm = await getDocument('fundRange', rangeID, 'TopupForm');
    return {
        keyFeatures,
        supplementaryInformationDocument,
        reportsAndAccounts,
        reportsAndAccounts2,
        prospectus,
        ISAApplicationForm,
        EURApplicationForm,
        USDApplicationForm,
        GBPApplicationForm,
        ISATransferForm,
        ISAConversionForm,
        OEICApplicationForm,
        OEICCorpTrustsForm,
        OEICSwitchForm,
        JISAApplicationForm,
        TopupForm,
    };
}

async function getFunds(rangeID) {
    const funds = await Funds.model.find().where('range', rangeID).sort('name').exec();

    const fundPromises = _.map(funds, async (fund) => {
        const price = await getPrice(fund.id);
        const className = await getClassName(fund.class);
        const documents = await getFundDocuments(fund.id);
        return {
            id: fund.id,
            manager: fund.managementTeam,
            description: fund.summary,
            name: fund.name,
            accInc: fund.accInc,
            objective: fund.objective,
            class: className,
            price: price.price,
            date: price.date,
            ISIN: fund.ISIN,
            launchDate: fund.launchDate ? dateFormat(new Date(fund.launchDate), 'dd/mm/yyyy') : 'N/A',
            iaSector: fund.iaSector,
            MorningstarSector: fund.MorningstarSector,
            NisaQualifying: fund.NisaQualifying,
            sedolNumber: fund.sedolNumber,
            documents,
        };
    });

    return await Promise.all(fundPromises)
}

async function getFundRanges(sponsorID) {
    const fundRanges = await Fundrange.model.find().where('from', sponsorID).sort('name').exec();

    const ranges = _.map(fundRanges, async (range) => {
     //   const documents = await getRangeDocuments(range.id);
        return {
            id: range.rangeID,
            name: range.name,
            category: range.marlboroughCategory,
            documentCategories: range.documentCategories,
            funds: _.keyBy(await getFunds(range.id), 'sedolNumber'),
        }
    });
    
    return await Promise.all(ranges);
}

async function getSponsors() {
    const sponsors = await Sponsors.model.find().sort('sortOrder').exec();

    const sponsorPromises = _.map(sponsors, async (sponsor) => {
        if (!(sponsor.International)) return false;
        const documents = await getSponsorDocuments(sponsor._id);
        console.log(sponsor.International);

        return {
             name: sponsor.name,
             international: sponsor.International,
             description: sponsor.description,
             logo: sponsor.logo.filename,
             banner: sponsor.ifslBannerImage.filename,
             website: sponsor.website,
             color: sponsor.color,
             valuationPoint: dateFormat(new Date(sponsor.validationDate), 'HH:MM'),
             fundRanges: _.keyBy(await getFundRanges(sponsor.id), 'id'),
             documents
        };
    });

    const sponsorsParsed = await Promise.all(sponsorPromises);

    return _(sponsorsParsed).filter('international').keyBy('name').value();
}


exports = module.exports = async function (req, res) {
    try {
        res.json(await getSponsors());
    } catch (err) {
        console.log(err);
    }
};
