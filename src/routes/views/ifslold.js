import dateFormat from 'dateformat';

var keystone = require('keystone');
var Sponsors = keystone.list('FundGroup');
var Funds = keystone.list('Fund');
var Fundrange = keystone.list('FundRange');
var FundClass = keystone.list('FundClass');
var Price = keystone.list('Price');
var Manager = keystone.list('Manager');

exports = module.exports = function (req, res) {
  var sponsors = {};
  
  var getSponsors = Sponsors.model.find().sort('sortOrder');
  getSponsors.exec().then(results => {
     return Promise.all(results.map(sponsor => {
         if (!sponsor.IFSL) return;
         sponsors[sponsor.name] = {
             description: sponsor.description,
             logo: sponsor.logo.filename,
             banner: sponsor.ifslBannerImage.filename,
             color: sponsor.color,
             valuationPoint: dateFormat(new Date(sponsor.validationDate), 'HH:MM'),
             fundRanges: {},
         };
         
         var getFundranges = Fundrange.model.find().where('from', sponsor.id).sort('sortOrder');
         return getFundranges.exec().then(results => {
           return Promise.all(results.map(fundrange => {
               sponsors[sponsor.name]['fundRanges'][fundrange.rangeID] = {
                   name: fundrange.name,
                   funds: {},   
               }
               
               var getFunds = Funds.model.find().where('range', fundrange.id).sort('sortOrder');
               return getFunds.exec().then(results => {
                   console.log('hi');
                   return Promise.all(results.map(fund => {
                     var getClass = FundClass.model.findById(fund.class);
                     var getPrice = Price.model.find().where('fund', fund.id).sort('-date').limit(1);
                     return getClass.exec().then(fundClass => {
                         sponsors[sponsor.name]['fundRanges'][fundrange.rangeID]['funds'][fund.sedolNumber] = {
                             name: fund.name,
                             accInc: fund.accInc,
                             class: fundClass.name,
                             ISIN: fund.ISIN,
                             sedolNumber: fund.sedolNumber,
                             documents: {
                               kiid: fund.kiid.filename,
                               factSheet: fund.factSheet.filename,
                               prospectus: fund.prospectus.filename,
                               reportsAndAccounts: fund.reportsAndAccounts.filename,
                               reportsAndAccounts2: fund.reportsAndAccounts2.filename,
                               ISAApplicationForm: fund.ISAApplicationForm.filename,
                               ISATransferForm: fund.ISATransferForm.filename,
                               ISAConversionForm: fund.ISAConversionForm.filename,
                               OEICApplicationForm: fund.OEICApplicationForm.filename,
                               OEICCorpTrustsForm: fund.OEICCorpTrustsForm.filename,
                               OEICConversionForm: fund.OEICConversionForm.filename,
                               OEICSwitchForm: fund.OEICSwitchForm.filename,
                               JISAApplicationForm: fund.JISAApplicationForm.filename,
                               TopupForm: fund.TopupForm.filename,
                             },
                         };
                     }).then(_ => getPrice.exec()).then(results => {
                        if (results[0]){
                            sponsors[sponsor.name]['fundRanges'][fundrange.rangeID]['funds'][fund.sedolNumber]['price'] = parseFloat(results[0].price).toFixed(4);
                            sponsors[sponsor.name]['fundRanges'][fundrange.rangeID]['funds'][fund.sedolNumber]['date'] = dateFormat(new Date(results[0].date), 'dd/mm/yyyy');
                        }
                     }, e => console.log(e));
                   }));
               });
           }));
         });         
     }));
  }).then(_ => {
      res.json(sponsors);
  }, e => console.log(e));
};
