import dateFormat from 'dateformat';
import _ from 'lodash';
import R from 'ramda';

var keystone = require('keystone');
var Sponsors = keystone.list('FundGroup');
var Funds = keystone.list('Fund');
var Fundrange = keystone.list('FundRange');
var FundClass = keystone.list('FundClass');
var Manager = keystone.list('Manager');
var ManagerCommentary = keystone.list('ManagerCommentary');
var Price = keystone.list('Price');
var Document = keystone.list('Document');
var ManagementTeam = keystone.list('ManagementTeam');

async function getClassName(classID) {
    const fundClass = await FundClass.model.findById(classID).exec();
    return fundClass.name;
}
async function getManagersComments(rangeID) {
     const managersComments = await ManagerCommentary.model.find().where('range', rangeID).sort('-publishedAt').limit(1).exec();
    return managersComments;
}

async function getPrice(fundID) {
    const fundPrice = await Price.model.find().where('fund', fundID).sort([['date', -1], ['_id', -1]]).limit(1).exec();

    const exists = fundPrice.length > 0;
    return ({
        price: exists && typeof fundPrice[0].price == 'number' ? fundPrice[0].price.toFixed(3) : 'N/A',
        bid: exists && typeof fundPrice[0].bid == 'number' ? fundPrice[0].bid.toFixed(3) : 'N/A',
        canc: exists && typeof fundPrice[0].canc == 'number' ? fundPrice[0].canc.toFixed(4) : 'N/A',
        yield: exists && fundPrice[0].yield ? fundPrice[0].yield : 'N/A',
        date: exists ? dateFormat(new Date(fundPrice[0].date), 'dd/mm/yyyy') : '00/00/0000',
    });
}

async function getDocument(table, tableID, docType, category) {
    let docQ;
    if (category != undefined) {
      docQ = await Document.model.find().where('type', docType).where('category', category).sort('-date').limit(1).exec();
    } else {
      docQ = await Document.model.find().where(table, tableID).where('type', docType).sort('-date').limit(1).exec();
    }
    return docQ.length > 0 ? docQ[0].name : undefined;
}


async function getFundDocuments(fundID,range,sponsorID) {
    const keyInvestmentInformationDocument = await getDocument('fund', fundID, 'kiid');
    const factSheet = await getDocument('fund', fundID, 'factsheet');
    let prospectus = await getDocument('fund', range.id, 'prospectus', range.documentCategories[0])
      if(typeof prospectus != 'undefined'){
        //i know this is not elegant, please don't look at me, I am ashamed
      }
      else{
        prospectus = await getDocument('fund', range.id, 'prospectus', range.documentCategories[1])
      }
    const reportsAndAccounts = await getDocument('fund', range.id, 'reportsAndAccounts', range.documentCategories[1])

    let consolidatedFactsheet = await getDocument('fund', range.id, 'consolidatedFactsheet', range.documentCategories[1])
      if(typeof consolidatedFactsheet != 'undefined'){
        //donothing
      }
      else{
        consolidatedFactsheet = await getDocument('fund', range.id, 'consolidatedFactsheet', range.documentCategories[0])
      }

    let reportsAndAccounts2 = await getDocument('fund', range.id, 'reportsAndAccounts2', range.documentCategories[1])
      if(typeof reportsAndAccounts2 != 'undefined'){
        //donothing
      }
      else{
        reportsAndAccounts2 = await getDocument('fund', range.id, 'reportsAndAccounts2', range.documentCategories[0])
      }
    let EURApplicationForm = await getDocument('fund', range.id, 'EURApplicationForm', range.documentCategories[0])
      if(typeof EURApplicationForm != 'undefined'){
        //donothing
      }
      else{
        EURApplicationForm = await getDocument('fund', range.id, 'EURApplicationForm', range.documentCategories[1])
      }
    let USDApplicationForm = await getDocument('fund', range.id, 'USDApplicationForm', range.documentCategories[0])
      if(typeof USDApplicationForm != 'undefined'){
        //donothing
      }
      else{
        USDApplicationForm = await getDocument('fund', range.id, 'USDApplicationForm', range.documentCategories[1])
      }
    let GBPApplicationForm =await getDocument('fund', range.id, 'GBPApplicationForm', range.documentCategories[0])
      if(typeof GBPApplicationForm != 'undefined'){
        //donothing
      }
      else{
        GBPApplicationForm = await getDocument('fund', range.id, 'GBPApplicationForm', range.documentCategories[1])
      }
    const keyFeatures = await getDocument('sponsor', sponsorID, 'keyFeatures')
    const supplementaryInformationDocument = await getDocument('sponsor', sponsorID, 'supplementaryInformationDocument')
//    const reportsAndAccounts = await getDocument('sponsor', sponsorID, 'reportsAndAccounts'),

    const ISAApplicationForm = await getDocument('sponsor', sponsorID, 'ISAApplicationForm')
    const ISATransferForm = await getDocument('sponsor', sponsorID, 'ISATransferForm')
    const ISAConversionForm = await getDocument('sponsor', sponsorID, 'ISAConversionForm')
    const OEICApplicationForm = await getDocument('sponsor', sponsorID, 'OEICApplicationForm')

    const OEICCorpTrustsForm = await getDocument('sponsor', sponsorID, 'OEICCorpTrustsForm')
    const OEICSwitchForm = await getDocument('sponsor', sponsorID, 'OEICSwitchForm')
    const OEICConversionForm = await getDocument('sponsor', sponsorID, 'OEICConversionForm')
    const JISAApplicationForm = await getDocument('sponsor', sponsorID, 'JISAApplicationForm')
    const JISATermsAndConditions = await getDocument('sponsor', sponsorID, 'JISATermsAndConditions')
    const ISATermsAndConditions = await getDocument('sponsor', sponsorID, 'ISATermsAndConditions')

    const TopupForm = await getDocument('sponsor', sponsorID, 'TopupForm')
    const APSForm= await getDocument('sponsor', sponsorID, 'APSForm')

    return {
      keyInvestmentInformationDocument,
      supplementaryInformationDocument,
      factSheet,
      prospectus,
      keyFeatures,
      consolidatedFactsheet,
      reportsAndAccounts,
      reportsAndAccounts2,
      EURApplicationForm,
      USDApplicationForm,
      GBPApplicationForm,
      ISAApplicationForm,
      ISATransferForm,
      ISAConversionForm,
      JISAApplicationForm,
      JISATermsAndConditions,
      OEICApplicationForm,
      OEICCorpTrustsForm,
      OEICConversionForm,
      OEICSwitchForm,
      JISAApplicationForm,
      ISATermsAndConditions,
      JISATermsAndConditions,
      TopupForm,
      APSForm
    };
}


async function getFunds(range,sponsor) {
  var sponsorID = sponsor.id
    const rangeID = range.id
    const funds = await Funds.model.find().where('range', rangeID).sort('sortOrder').exec();
    const managersComments = await getManagersComments(rangeID)

    const fundPromises = _.map(funds, async (fund) => {
        const price = await getPrice(fund.id);
        const className = await getClassName(fund.class);
        //const managerCommentary = await getManagersComments(fund.id, range, sponsorID)
      //  const documents = await getFundDocuments(fund.id,range,sponsorID);
        const team = await getTeam(fund.managementTeam);
        const time = dateFormat(new Date(sponsor.validationDate), 'HH:MM');
        return {
            id: fund.id,
            manager: fund.managementTeam,
            team: team,
            description: fund.summary,
            name: fund.name,
            dw: fund.dW,
            utoeic: fund.utOeic,
            accInc: fund.accInc,
            objective: fund.objective,
            class: className,
            time: time,
            price: price.price,
            canc: price.canc,
            yield: price.yield,
            bid: price.bid,
            date: price.date,
            ISIN: fund.ISIN,
            launchDate: fund.launchDate ? dateFormat(new Date(fund.launchDate), 'dd/mm/yyyy') : 'N/A',
            iaSector: fund.iaSector,
            MorningstarSector: fund.MorningstarSector,
            NisaQualifying: fund.NisaQualifying,
            sedolNumber: fund.sedolNumber,
        //    documents,
            managersComments
        };
    });

    return await Promise.all(fundPromises)
}

async function getFundRanges(sponsor) {
  var sponsorID = sponsor.id
    const fundRanges = await Fundrange.model.find().where('from', sponsorID).sort('sortOrder').exec();

    const ranges = _.map(fundRanges, async (range) => {
        //const documents = await getRangeDocuments(range.id);
        return {
            id: range.rangeID,
            name: range.name,
            category: range.marlboroughCategory,
            documentCategories: range.documentCategories,
            funds: _.keyBy(await getFunds(range,sponsor), 'sedolNumber'),
        }
    });

    return await Promise.all(ranges);
}


async function getTeam(teamID) {
  const managementTeam = await ManagementTeam.model.findOne(teamID).populate('people').exec();
  return managementTeam
}

async function getSponsors(req,res) {
    const sponsor = await Sponsors.model.findOne({'name':req.params.sponsorName}).exec();
        return   _.groupBy(_.keyBy(await getFundRanges(sponsor), 'name'),"category");

    return   await Promise.all(sponsorPromises);

}


exports = module.exports = async function (req, res) {
    try {
      res.json(await getSponsors(req));
    } catch (err) {
        console.log(err);
    }
};
