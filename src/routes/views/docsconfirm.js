import R from 'ramda';

var keystone = require('keystone');
var csv = require('csv');
var fs = require('fs');
var _ = require('lodash');
var moment = require('moment');

var Fund = keystone.list('Fund');
var FundRange = keystone.list('FundRange');
var Sponsor = keystone.list('FundGroup');


function fileExists(path) {
  try {
    let stats = fs.lstatSync(path);
    if (stats.isFile()) {
        return true;
    }
    return false;
  } catch (e) {
    return false;
  }
  return false;
}

const documentTypes = {
'kiid': [],
'factsheet': [],
'consolidatedFactsheet': [],
'keyFeatures': [],
'supplementaryInformationDocument': [],
'reportsAndAccounts': ['reportsAndAccountsShort'],
'reportsAndAccounts2': ['reportsAndAccountsLong'],
'prospectus': [],
'ISAApplicationForm': [],
'EURApplicationForm': [],
'GBPApplicationForm': [],
'USDApplicationForm': [],
'ISATransferForm': [],
'JISATermsAndConditions': [],
'ISAConversionForm': [],
'OEICApplicationForm': [],
'OEICCorpTrustsForm': [],
'OEICSwitchForm': [],
'OEICConversionForm': [],
'JISAApplicationForm': [],
'TopupForm': [],
'APSForm': [],
'SupplementalProspectus':[],
'ISAKeyFeatures':[],
'TermsAndConditions':[],
'Supplement':[],
'ISAApplicationFormUserGuide':[],
'ISATransferApplicationFormUserGuide':[],
'JuniorISAFormUserGuide':[],
'APSInvestmentUserGuide':[],
'APSTransferUserGuide':[],
'FacilitatedAdviserChargingDocuments':[],
'OEICApplicationFormUserGuide':[],
'CorpTrustsFormUserGuide':[],
'WithdrawalForm':[],
'WithdrawalFormUserGuide':[],
'TopUpAndRegularSaverAmendmentForm':[],
'TopUpAndRegularSaverAmendmentFormUserGuide':[],
'RegularWithrawalForm':[],
'ControllingPersonSelfCertificationForm':[],
'EntitySelfCertificationForm':[],
'IndividualSelfCertificationForm':[]
}

const fundDocuments = ['kiid', 'factsheet'];

exports = module.exports = async function(req, res) {
    var view = new keystone.View(req, res);
    var locals = res.locals;
    try {
        const files = R.when(R.complement(R.isArrayLike), R.of)(req.files.files);
        const results = R.map(async (file) => {
            if (!R.contains('-')(file.originalname) || R.contains('.php')(file.path) ) {
                return {fail: true, message: 'Not Recognised: ' + file.originalname}
            }

            const split = R.split('-', file.originalname);
            const name = split[0];
            const userType = R.split('.', split[split.length-1])[0];
            const category = split.length == 3 ? split[1] : '';

            const userTypeSanitized = userType.toLowerCase().replace(/\s+/g, '')

            const foundType = R.pickBy((others, type) => {
              console.log(type);
              if (type.toLowerCase() == userTypeSanitized) return true;
              if (R.find(t => t.toLowerCase() == userTypeSanitized)(others)) return true;
              return false;
            })(documentTypes);

            if (R.isEmpty(foundType)) return {fail: true, message: 'Document Type Not Recognised: (' + userType + ') in ' +  file.originalname};
            const type = R.keys(foundType)[0];


            const path = file.path;

            const foundFund = await Fund.model.findOne({sedolNumber: name}).exec();
            const foundSponsor = await Sponsor.model.findOne({name: name}).exec();

            if (!foundFund && !foundSponsor) {
                return {fail: true, message: 'No Matching Fund/Sponsor Found: ' + file.originalname}
            }
            if (foundFund && !R.contains(type, fundDocuments)) {
                return {fail: true, message: 'Funds can not have a ' + userType  + ', only Shareclasses can, check the filename: ' + file.originalname}
            }
            if (foundSponsor && R.contains(type, fundDocuments)) {
                return {fail: true, message: 'Sponsors can not have a ' + userType  + ', only Funds can, check the filename: ' + file.originalname}
            }

            const fund = foundFund ? foundFund.id : undefined;
            const sponsor = foundSponsor ? foundSponsor.id : undefined;
            const sharename = foundFund ? foundFund.name : foundSponsor.name;
            const sedol = foundFund ? foundFund.sedolNumber : undefined;

            return {
                fund, sponsor, type, path, sharename, sedol, category,
                original: file.originalname
            }

        })(files);
        locals.results = await Promise.all(results);
        view.render('docsconfirm');
    } catch (e) {
        console.log(e);
    }
};
