var keystone = require('keystone');
var R = require('ramda');

var User = keystone.list('User');

exports = module.exports = async function(req, res) {
    const email = req.body.email;
    const password = req.body.password;
    const site = req.body.site;

    const user = await User.model.findOne().where('email', email).exec();
    if (!user) return res.json({err: 'User Not Found'});

    const passwordsMatch = await new Promise((res) => {
        user._.password.compare(password, (err, isMatch) => {
            if (!err && isMatch)
                return res(true);
            return res(false);
        });
    });
    if (!passwordsMatch) return res.json({err: 'Password Incorect'});

    let hasAccess = false;
    switch(site) {
        case 'ifsl':
            hasAccess = user.ifsl;
            break;
        case 'marlborough':
            hasAccess = user.marlborough;
            break;
    }

    if (!hasAccess) return res.json({err: 'No Acess To This Site'});

    return res.json({err: false, success: true})
};
