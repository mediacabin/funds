import dateFormat from 'dateformat';
import _ from 'lodash';
import R from 'ramda';

var keystone = require('keystone');
var Sponsors = keystone.list('FundGroup');
var Funds = keystone.list('Fund');
var Fundrange = keystone.list('FundRange');
var FundClass = keystone.list('FundClass');
var Document = keystone.list('Document');


const fundDocuments = ['kiid', 'factsheet'];
const rename = {
    reportsAndAccountsShort: 'reportsAndAccounts',
    reportsAndAccountsLong: 'reportsAndAccounts2',
}

async function getDocument(table, tableID, docType, category) {
    let docQ;
    if (category != undefined) {
      docQ = await Document.model.find().where(table, tableID).where('type', docType).where('category', category).sort('-date').limit(1).exec();
    } else {
      docQ = await Document.model.find().where(table, tableID).where('type', docType).sort('-date').limit(1).exec();
    }
    return docQ.length > 0 ? docQ[0].name : undefined;
}

const getDocumentForSponsor = async (sponsor, category, typeId) => {
    if (category) {
        const sponsorDoc = await getDocument('sponsor', sponsor, typeId, category);
        if (sponsorDoc) return sponsorDoc;
        throw new Error('400');
    } 
    
    const sponsorDoc = await getDocument('sponsor', sponsor, typeId);
    if (sponsorDoc) return sponsorDoc;
    throw new Error('400');
}

const getDocumentForFund = async (fund, typeId) => {
    if (R.contains(typeId, fundDocuments)) {
        const doc = await getDocument('fund', fund, typeId);
        if (!doc) throw new Error('400');
        return doc;
    }
    const fundGroup = await Fundrange.model.findById(fund.range).exec();
    const fundSponsor = await Sponsors.model.findById(fundGroup.from).exec();

    const categoryDoc = R.pipe(
        R.map((cat) => getDocument('sponsor', fundSponsor, typeId, cat)),
        x => Promise.all(x),
        R.find(x => x != undefined)
    )(fundGroup.documentCategories);

    if (categoryDoc) return categoryDoc;
    const sponsorDoc = await getDocument('sponsor', fundSponsor, typeId);
    if (sponsorDoc) return sponsorDoc;
    throw new Error('400');
}

exports = module.exports = async function (req, res) {
    let { fundId, typeId, categoryId } = req.params;
    if (rename[typeId]) typeId = rename[typeId];

    const fund = await Funds.model.findOne().where('sedolNumber', fundId).exec();
    if (fund) {
        try {
            const doc = await getDocumentForFund(fund, typeId);
            return res.sendFile(process.env.UPLOAD_LOCATION + "/documents/" + doc);
        } catch (e) {
            return res.sendStatus('404');
        }
    } 

    const foundSponsor = await Sponsors.model.findOne({name: fundId}).exec();
    if (foundSponsor) {
        try {
            const doc = await getDocumentForSponsor(foundSponsor, categoryId, typeId);
            return res.sendFile(process.env.UPLOAD_LOCATION + "/documents/" + doc);
        } catch (e) {
            return res.sendStatus('404');
        }
    } 


    return res.sendStatus('404');
};
