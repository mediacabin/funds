var _ = require('lodash');

require('dotenv').load();

/**
	Initialises the standard view locals
*/

exports.initLocals = function(req, res, next) {

	var locals = res.locals;

	locals.navLinks = [
		{ label: 'Home',		key: 'home',		href: '/' }
	];

	locals.user = req.user;

	next();

};

/*
* Allow cross origin
*/
exports.setCors = function (req,res,next) {
	if(
		req.get('origin') ==  "http://marlboroughinternational.gg" ||
		req.get('origin') == "http://mpsdemo.xyz" || req.get('origin') == "http://www.mps.marlboroughinvests.com" ||
		req.get('origin') == "http://www.marlboroughfunds.com" || req.get('origin') == "http://marlboroughfunds.com" ||
		req.get('origin') == "http://ufcdemo.xyz" || req.get('origin') == "http://ufcfunds.com" ||
		req.get('origin') == "http://ifsldemo.xyz" || req.get('origin') == "http://ifslfunds.com" ||
		req.get('origin') == "http://investsdemo.xyz" || req.get('origin') == "http://marlboroughinvests.com" ||
		req.get('origin') == "http://globalnetdemo.xyz" ||
		req.get('origin') == "https://marlboroughinternational.gg" ||
		req.get('origin') == "https://mpsdemo.xyz" || req.get('origin') == "https://www.mps.marlboroughinvests.com" ||
		req.get('origin') == "https://www.marlboroughfunds.com" || req.get('origin') == "https://marlboroughfunds.com" ||
		req.get('origin') == "https://ufcdemo.xyz" || req.get('origin') == "https://ufcfunds.com" ||
		req.get('origin') == "https://ifsldemo.xyz" || req.get('origin') == "https://ifslfunds.com" ||
		req.get('origin') == "https://investsdemo.xyz" || req.get('origin') == "https://marlboroughinvests.com" ||
		req.get('origin') == "https://globalnetdemo.xyz" || req.get('origin') == "http://localtest.xyz:3000"
	){
		res.header('Access-Control-Allow-Origin', req.get('origin'));
    res.header('Access-Control-Allow-Methods', 'GET');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
	}
		next();
}

/**
	Fetches and clears the flashMessages before a view is rendered
*/

exports.flashMessages = function(req, res, next) {

	var flashMessages = {
		info: req.flash('info'),
		success: req.flash('success'),
		warning: req.flash('warning'),
		error: req.flash('error')
	};

	res.locals.messages = _.some(flashMessages, function(msgs) { return msgs.length; }) ? flashMessages : false;

	next();

};


/**
	Prevents people from accessing protected pages when they're not signed in
 */

exports.requireUser = function(req, res, next) {

	if (!req.user) {
		req.flash('error', 'Please sign in to access this page.');
		res.redirect('/keystone/signin');
	} else {
		next();
	}

};
