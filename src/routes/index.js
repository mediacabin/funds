var keystone = require('keystone')
var middleware = require('./middleware')
var importRoutes = keystone.importer(__dirname)

// var Raven = require('raven');
// Raven.config('https://f0179885eec74d61b5e252fa70dbf6dd:a7b9abd8412948b7ae8643ef870764c7@sentry.io/158154', {
//     autoBreadcrumbs: true
// }).install();
var router = require('express-promise-router')();

//keystone.pre('routes', Raven.requestHandler());
keystone.pre('routes', middleware.initLocals)
keystone.pre('render', middleware.flashMessages)
// keystone.set('500', (err, req, res, next) => {
//   Raven.context(function () {
//     Raven.setContext({
//       extra: {
//         req: req,
//         res: res,
//         err: err,
//       }
//     });
//     Raven.captureException(err);
//   });
//   console.log(err);
//   res.status(500).send('Server Error');
// });

var routes = {
  views: importRoutes('./views')
}

exports = module.exports = function (app) {
  router.get('/', routes.views.index);
  router.get('/ifsl.json', routes.views.ifsl);
  router.get('/marlboroughfunds.json', routes.views.marlborough);
  router.get('/getfunds/:sponsorName.json',middleware.setCors, routes.views.simpleMarlborough);

  router.get('/getliterature/:sponsorName',middleware.setCors, routes.views.simpleLiterature);
  router.get('/getsponsor/:sponsorName',middleware.setCors, routes.views.mfm);
  router.get('/fetchsponsor/:sponsorName',keystone.middleware.api, routes.views.mfm);

  router.get('/international.json', routes.views.international);

  router.get('/csv', middleware.requireUser, routes.views.csvform);
  router.post('/csv', middleware.requireUser, routes.views.csv);
  router.post('/csvcommit', middleware.requireUser, routes.views.csvcommit);
  router.all('/managers-comments', middleware.requireUser, routes.views.managersCommentsForm);


  router.get('/docs', middleware.requireUser, routes.views.docsform);
  router.post('/docs', middleware.requireUser, routes.views.docsconfirm);
  router.post('/docscomplete', middleware.requireUser, routes.views.docscomplete);

  router.post('/remoteLogin', routes.views.remoteLogin);

  router.get('/doc/:fundId/:typeId.pdf', routes.views.docproxy);
  router.get('/doc/:fundId/:categoryId/:typeId.pdf', routes.views.docproxy);

router.get('/doclean', middleware.requireUser, routes.views.docCleanup);

  app.use('/', router);
}
